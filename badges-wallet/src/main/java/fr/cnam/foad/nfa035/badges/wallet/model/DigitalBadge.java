package fr.cnam.foad.nfa035.badges.wallet.model;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge implements Comparable {

    private DigitalBadgeMetadata metadata;
    private Date begin;
    private Date end;
    private String serial;
    private File badge;
    private static final Logger Log = LogManager.getLogger(WalletDeserializerDirectAccessImpl.class);

    /**
     * Constructeur
     *
     * @param metadata
     * @param badge
     */
    public DigitalBadge(DigitalBadgeMetadata metadata, File badge) {
        this.metadata = metadata;
        this.badge = badge;

    }

    /**
     * Constructeur
     *
     * @param serial  : unique serial number
     * @param metadata : meta données
     * @param begin : début de validité
     * @param end : fin de validité
     * @param badge : fichier contenant l'image du badge
     */
    public DigitalBadge(String serial,Date begin, Date end, DigitalBadgeMetadata metadata, File badge) {
        this.serial = serial;
        this.begin = begin;
        this.end = end;
        this.metadata = metadata;
        this.badge = badge;
    }
    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */

    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }
    /**
     * Getter de begin
     * @return begin (date de début de validité du badge)
     */
    public Date getBegin() {
        return begin;
    }
    /**
     * Setter de begin (Date de debut)
     * @param begin
     */
    public void setBegin(Date begin) {
        this.begin = begin;
    }
    /**
     * Getter de end
     * @return end (date de fin de validité du badge)
     */
    public Date getEnd() {
        return end;
    }
    /**
     * Setter de end (Date de fin de validité du badge)
     * @param end
     */
    public void setEnd(Date end) {
        this.end = end;
    }
    /**
     * Getter de serial
     * @return serial (Numero de serie)
     */
    public String getSerial() {
        return serial;
    }
    /**
     * Setter de serial (Numero de serie )
     * @param serial
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return metadata.equals(that.metadata) && badge.equals(that.badge) &&
                (that.getSerial() == serial) && (that.begin == begin) && (that.end == end);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge, begin, end , serial);
    }
    /**
     * {@inheritDoc}
     *
     * @param o
     * @return int
     */
    @Override
    public int compareTo(Object o) {
        int result = 1;
        if (this == o) result = 0;
        if (o == null || getClass() != o.getClass()) result = 0;
        if (result == 1) {


            DigitalBadge digitalBadge = (DigitalBadge) o;
            result = metadata.compareTo(digitalBadge.getMetadata());
            Log.info("DigitalBadge.compareTo serial " + this.serial + "digitalBadge.serial:" +  digitalBadge.getSerial() + "result " + result);

        }
        return result;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", valide du " + begin + " au " + end +
                ", serial" + serial +
                ", badge=" + badge +
                '}';
    }
}
