package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

/**
 * POJO model représentant les Métadonnées d'un Badge Digital
 */
public class DigitalBadgeMetadata implements Comparable {

    private int badgeId;
    private long walletPosition;
    private long imageSize;

    /**
     * Constructeur
     *
     * @param badgeId
     * @param imageSize
     */
    public DigitalBadgeMetadata(int badgeId, long walletPosition, long imageSize) {
        this.badgeId = badgeId;
        this.walletPosition = walletPosition;
        this.imageSize = imageSize;
    }

    /**
     * Getter du badgeId
     * @return l'id du badge
     */
    public int getBadgeId() {
        return badgeId;
    }

    /**
     * Setter du badgeId
     * @param badgeId
     */
    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    /**
     * Getter de la taille de l'image
     * @return long la taille de l'image
     */
    public long getImageSize() {
        return imageSize;
    }

    /**
     * Setter de l'imageSize
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    /**
     * Getter de la position du badge dans le wallet
     * @return long la position du badge dans le wallet
     */
    public long getWalletPosition() {
        return walletPosition;
    }

    /**
     * Setter de la position du badge dans le wallet
     * @param walletPosition
     */
    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    /**
     * {@inheritDoc}
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return badgeId == that.badgeId && walletPosition == that.walletPosition && imageSize == that.imageSize;
    }
    /**
     * {@inheritDoc}
     *
     * @param o
     * @return int
     */
    @Override
    public int compareTo(Object o) {
        int result = 1;
        if (this == o) result = 0;
        if (o == null || getClass() != o.getClass()) result = 0;
        if (result == 1) {
            DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
            if (this.walletPosition  > that.walletPosition) {
                result = 1;
            } else if (this.walletPosition  < that.walletPosition) {
                result = -1;
            } else {
                result = 0;
            }

        }
        return result;

    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(badgeId, walletPosition, imageSize);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeId=" + badgeId +
                ", imageSize=" + imageSize +
                '}';
    }


}
